How to start import of historical files to a jobsearch environment running in OpenShift


## Start a manual PipelineRun

1. Download the [Openshift CLI (oc)](https://console-openshift-console.apps.testing.services.jtech.se/command-line-tools)

2. Login to Openshift with oc  (first login to https://console-openshift-console.apps.testing.services.jtech.se/ and click on your logged in username in the upper right corner, and click "Copy login command"). Click 'Display Token' in the web browser and copy the "Log in with this token"-command (e.g. oc login --token=sha256...)

3. Paste that line in a terminal and press enter. 

4. Select the correct jobsearch-apis-* project of the available OpenShift projects. Type `oc project PROJECT_NAME` to select project, `oc project` to verify. 

5. Change year in `historical-import-pipelinerun.yaml`, in the examples directory

6. Start pipelinerun: `oc create -f historical-import-pipelinerun.yaml`

7. Check progress in OpenShift  `https://console-openshift-console.apps.testing.services.jtech.se/pipelines/ns/PROJECT_NAME/pipeline-runs`